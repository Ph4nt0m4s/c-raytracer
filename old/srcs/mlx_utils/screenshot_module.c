/*
** screenshot_module.c for raytracer in
** /home/mazier_j/MUL_2014_rtracer/srcs/mlx_utils
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:14:09 2015 Joffrey Mazier
** Last update Sun Jun  7 16:39:58 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include "keymap.h"
#include "define_file.h"
#include "rt1.h"

char		*file_name_sc()
{
  char		*file_name;
  int		i;
  int		ret;

  i = 1;
  if ((file_name = malloc((sizeof(char)) * (50))) == NULL)
    return (NULL);
  sprintf(file_name, "screenshot%d.jpeg", i);
  while ((ret = access(file_name, F_OK)) == 0)
    sprintf(file_name, "screenshot%d.jpeg", ++i);
  return (file_name);
}

void		screen_shot_it2(char *tmp3, int j, char *cmde)
{
  int		i;

  i = 0;
  while (tmp3[i] != '\0')
    cmde[j++] = tmp3[i++];
  cmde[j] = '\0';
  system(cmde);
  free(cmde);
}

int		screen_shot_it()
{
  char		*cmde;
  char		*tmp;
  char		*tmp2;
  char		*tmp3;
  int		i;
  int		j;

  i = 0;
  j = 0;
  if ((cmde = malloc((sizeof(char)) * (100))) == NULL)
    return (-1);
  tmp = "import -window ";
  tmp2 = WIN_NAME;
  while (tmp[i] != '\0')
    cmde[j++] = tmp[i++];
  i = 0;
  while (tmp2[i] != '\0')
    cmde[j++] = tmp2[i++];
  if ((tmp3 = file_name_sc()) == NULL)
    return (-1);
  cmde[j++] = ' ';
  screen_shot_it2(tmp3, j, cmde);
  printf("Screenshot saved in : %s\n", tmp3);
  return (0);
}
