/*
** gere_resize.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/mlx_utils
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Sat Jun  6 17:29:06 2015 Joffrey Mazier
** Last update Sat Jun  6 17:57:09 2015 Joffrey Mazier
*/

#include "rt1.h"
#include "define_file.h"

void		gere_resize(int key, t_pars *util)
{
  if (key == UP)
    {
      if (util->obj[util->eve->select[0]]->inter == inter_cone)
	util->obj[util->eve->select[0]]->extra += DEGTORAD(1);
      else
	util->obj[util->eve->select[0]]->extra += 1;
    }
  else if (key == DOWN)
    {
      if (util->obj[util->eve->select[0]]->inter == inter_cone)
	util->obj[util->eve->select[0]]->extra -= DEGTORAD(1);
      else
	util->obj[util->eve->select[0]]->extra -= 1;
    }
}
