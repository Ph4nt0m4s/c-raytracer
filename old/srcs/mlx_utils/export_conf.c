/*
** export_conf.c for rtracer in /home/mazier_j/MUL_2014_rtracer/srcs/mlx_utils
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Wed Jun  3 13:25:42 2015 Joffrey Mazier
** Last update Sat Jun  6 14:49:42 2015 Joffrey Mazier
*/

#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <math.h>
#include <errno.h>
#include "rt1.h"
#include "define_file.h"

char		*create_directory(int times, char *str)
{
  char		*nb;
  int		i;
  int		j;

  i = 0;
  if (str != NULL)
    free(str);
  if ((str = malloc((sizeof(char)) * (times + 5))) == NULL)
    return (NULL);
  if ((nb = malloc((sizeof(char)) * (times + 2))) == NULL)
    return (NULL);
  str[0] = 'e';
  str[1] = 'x';
  str[2] = 'p';
  str[3] = '_';
  j = 4;
  sprintf(nb, "%d", times);
  while (nb[i] != '\0')
    str[j++] = nb[i++];
  str[j] = '\0';
  free(nb);
  return (str);
}

char		*my_str_cat(char *str1, char *str2)
{
  int		i;
  int		j;
  char		*tmp;

  i = 0;
  j = 0;
  if ((tmp = malloc((sizeof(char)) * (my_strlen(str1)
				      + my_strlen(str2) + 2))) == NULL)
    return (NULL);
  while (str1[i] != '\0')
    tmp[j++] = str1[i++];
  tmp[j++] = '/';
  i = 0;
  while (str2[i] != '\0')
    tmp[j++] = str2[i++];
  tmp[j] = '\0';
  return (tmp);
}

int		export_da_conf(t_pars *util)
{
  char		*dir_name;
  int		times;

  times = 1;
  dir_name = NULL;
  if ((dir_name = create_directory(times, dir_name)) == NULL)
    return (-1);
  while ((mkdir(dir_name, 0777)) == -1)
    {
      if (errno != EEXIST)
	return (my_puterror("I can't create export's directory.. abort !\n"));
      times++;
      if ((dir_name = create_directory(times, dir_name)) == NULL)
	return (-1);
    }
  if ((create_obj(util, dir_name)) == -1)
    return (-1);
  if ((create_cam(util, dir_name)) == -1)
    return (-1);
  if ((create_lux(util, dir_name)) == -1)
    return (-1);
  printf("Exportation successfull ! Saved in %s.\n", dir_name);
  free(dir_name);
  return (0);
}
