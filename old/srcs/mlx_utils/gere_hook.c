/*
** gere_hook.c for Wolf3D in /home/cadet_g/tp/Graph/Wolf3D/srcs/V1
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Wed Dec 17 18:15:57 2014 Gabriel CADET
** Last update Tue Jul 05 13:42:07 2016 Gabriel CADET
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "mlx.h"
#include "keymap.h"
#include "define_file.h"
#include "rt1.h"

int		gere_expose(t_pars *util)
{
  mlx_put_image_to_window(util->img->mlx, util->img->win,
			  util->img->img, 0, 0);
  return (0);
}

void		use_another_image(t_pars *util)
{
  util->img->img = mlx_new_image(util->img->mlx, XWIN, YWIN);
  util->img->addr = mlx_get_data_addr(util->img->img, &util->img->bpp,
				      &util->img->line, &util->img->endian);
  mlx_put_image_to_window(util->img->mlx,
			  util->img->win, util->img->img, 0, 0);
}

void		change_mode(int key, t_pars *util)
{
  if (key == SQUARE_T)
    {
      if (util->eve->select[1] == MAX_MODE)
	util->eve->select[1] = MIN_MODE;
      else
	util->eve->select[1]++;
      util->eve->select[0] = 0;
      printf("Switch to mode N°%d : ", util->eve->select[1]);
      printf("%s\n", MODE_OBJ(util->eve->select[1]));
    }
  else if (key == KEY_1 && util->eve->mode != TRANSLATION)
    {
      util->eve->mode = 1;
      printf("Switch to mode %s\n", MODE(util->eve->mode));
    }
  else if (key == KEY_2 && util->eve->mode != ROTATION)
    {
      util->eve->mode = 2;
      printf("Switch to mode %s\n", MODE(util->eve->mode));
    }
  else if (key == KEY_4 && util->eve->mode != RESIZE)
    {
      util->eve->mode = 4;
      printf("Switch to mode %s\n", MODE(util->eve->mode));
    }
}

int		gere_key(int key, t_pars *util)
{
  if (key == EXPORT)
    if ((export_da_conf(util)) == -1)
      my_puterror("Can't export file, an error happened !\n");
  if (key == SCREEN_S)
    screen_shot_it();
  if (key == KEY_ESC)
    {
      mlx_destroy_window(util->img->mlx, util->img->win);
      exit(EXIT_SUCCESS);
    }
  change_mode(key, util);
  if (key == FASTER || key == NICER)
    use_another_image(util);
  if (key == FASTER && util->img->faster == 1)
    util->img->faster = 8;
  else if (key == NICER && util->img->faster == 8)
    util->img->faster = 1;
  if (util->eve->select[1] == OBJECT)
    gere_key_obj(key, util);
  else if (util->eve->select[1] == CAMERA)
    gere_key_cam(key, util);
  else if (util->eve->select[1] == LUX)
    gere_key_lux(key, util);
  return (0);
}
