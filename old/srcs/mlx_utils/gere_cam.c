/*
** gere_cam.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/mlx_utils
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:15:48 2015 Joffrey Mazier
** Last update Tue Jul 05 13:41:28 2016 Gabriel CADET
*/

#include <stdio.h>
#include "mlx.h"
#include "keymap.h"
#include "define_file.h"
#include "rt1.h"

void		gere_key_cam_translation(int key, t_pars *util)
{
  if (key == UP)
    util->eye[util->eve->select[0]]->z += 20;
  else if (key == DOWN)
    util->eye[util->eve->select[0]]->z -= 20;
  else if (key == LEFT)
    util->eye[util->eve->select[0]]->y += 20;
  else if (key == RIGHT)
    util->eye[util->eve->select[0]]->y -= 20;
  else if (key == P_UP)
    util->eye[util->eve->select[0]]->x += 20;
  else if (key == P_DOWN)
    util->eye[util->eve->select[0]]->x -= 20;
}

void		gere_key_cam_rotation(int key, t_pars *util)
{
  if (key == UP)
    util->eye[util->eve->select[0]]->angley -= 0.2;
  else if (key == DOWN)
    util->eye[util->eve->select[0]]->angley += 0.2;
  else if (key == LEFT)
    util->eye[util->eve->select[0]]->anglez += 0.2;
  else if (key == RIGHT)
    util->eye[util->eve->select[0]]->anglez -= 0.2;
  else if (key == P_UP)
    util->eye[util->eve->select[0]]->anglex += 0.2;
  else if (key == P_DOWN)
    util->eye[util->eve->select[0]]->anglex -= 0.2;
}

void		gere_key_cam(int key, t_pars *util)
{
  if (util->eve->mode == ROTATION)
    gere_key_cam_rotation(key, util);
  else if (util->eve->mode == TRANSLATION)
    gere_key_cam_translation(key, util);
  if (key == TABUL)
    {
      if (util->eve->select[0] < util->nb_cam - 1)
        util->eve->select[0]++;
      else
        util->eve->select[0] = 0;
      printf("Object n°%d\n", util->eve->select[0]);
      printf("Object selected : %s\n", util->eye[util->eve->select[0]]->name);
      util->img->cam = util->eve->select[0];
    }
  if (key == UP || key == DOWN || key == KEY_ESC ||
      key == LEFT || key == RIGHT || key == P_UP ||
      key == P_DOWN || key == TABUL)
    {
      get_image(util->img, util->eye[util->img->cam], util->obj, util->spot);
      mlx_put_image_to_window(util->img->mlx,
                              util->img->win, util->img->img, 0, 0);
    }
}
