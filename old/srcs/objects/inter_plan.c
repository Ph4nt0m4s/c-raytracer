/*
** inter_plan.c for rtv1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Sun Mar 22 19:12:21 2015 Gabriel CADET
** Last update Sat Jun  6 17:45:18 2015 Joffrey Mazier
*/

#include <math.h>
#include "rt1.h"

double		inter_limited_plan(t_obj *plan, double distance, double *dist)
{
  if (plan->limit[1] == 1)
    {
      if (dist[0] > plan->limit[0])
	return (-1);
      if (dist[1] > plan->limit[0])
	return (-1);
      if (dist[2] > plan->limit[0])
	return (-1);
    }
  else if (plan->limit[1] == 2)
    if (distance > plan->limit[0])
      return (-1);
  return (0);
}

double		inter_plan(t_eye *eye, t_obj *plan)
{
  double	k;
  double	point[3];
  double	dist[3];
  double	distance;

  if (!eye->vx && !eye->vy && !eye->vz)
    return (-1);
  k = -(plan->vx * (eye->x - plan->x)
	+ plan->vy * (eye->y - plan->y)
	+ plan->vz * (eye->z - plan->z))
    / (plan->vx * eye->vx + plan->vy * eye->vy + plan->vz * eye->vz);
  point[0] = eye->x + eye->vx * k;
  point[1] = eye->y + eye->vy * k;
  point[2] = eye->z + eye->vz * k;
  dist[0] = SQR(plan->x - point[0]);
  dist[1] = SQR(plan->y - point[1]);
  dist[2] = SQR(plan->z - point[2]);
  distance = sqrt(dist[0] + dist[1] + dist[2]);
  dist[0] = sqrt(dist[0]);
  dist[1] = sqrt(dist[1]);
  dist[2] = sqrt(dist[2]);
  if (inter_limited_plan(plan, distance, dist) == -1)
    return (-1);
  return (k);
}
