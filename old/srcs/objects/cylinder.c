/*
** cylinder.c for raytracer in
**  /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:21:13 2015 Gabriel CADET
** Last update Sat Jun  6 14:32:49 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "xfiles.h"
#include "rt1.h"

t_obj	*init_cylinder()
{
  t_obj	*new_cylinder;

  if (!(new_cylinder = xmalloc(sizeof(t_obj))))
    return (NULL);
  new_cylinder->set_x = cylinder_set_x;
  new_cylinder->set_y = cylinder_set_y;
  new_cylinder->set_z = cylinder_set_z;
  new_cylinder->vector_x = cylinder_vector_x;
  new_cylinder->vector_y = cylinder_vector_y;
  new_cylinder->vector_z = cylinder_vector_z;
  new_cylinder->set_color = cylinder_set_color;
  new_cylinder->set_extra = cylinder_set_radius;
  new_cylinder->set_bril = cylinder_set_bril;
  new_cylinder->inter = inter_cylinder;
  new_cylinder->get_normal = normal_cylinder;
  new_cylinder->destroy = destroy_objs;
  return (new_cylinder);
}

int	cylinder_set_x(t_obj *cylinder, double value)
{
  cylinder->x = value;
  return (0);
}

int	cylinder_set_y(t_obj *cylinder, double value)
{
  cylinder->y = value;
  return (0);
}

int	cylinder_set_z(t_obj *cylinder, double value)
{
  cylinder->z = value;
  return (0);
}

int	cylinder_set_radius(t_obj *cylinder, double value)
{
  cylinder->extra = value;
  return (0);
}
