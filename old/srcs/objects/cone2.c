/*
** cone2.c for raytracer in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:21:13 2015 Gabriel CADET
** Last update Fri Jun  5 22:53:06 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "rt1.h"

int	cone_set_color(t_obj *cone, int color)
{
  cone->color = color;
  return (0);
}

int	cone_vector_x(t_obj *cone, double value)
{
  cone->vx = value;
  return (0);
}

int	cone_vector_y(t_obj *cone, double value)
{
  cone->vy = value;
  return (0);
}

int	cone_vector_z(t_obj *cone, double value)
{
  cone->vz = value;
  return (0);
}

int	cone_set_bril(t_obj *cone, double value)
{
  cone->brillance = value;
  return (0);
}
