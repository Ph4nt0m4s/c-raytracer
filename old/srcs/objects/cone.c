/*
** cone.c for raytracer in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:21:13 2015 Gabriel CADET
** Last update Fri Jun  5 22:52:57 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "xfiles.h"
#include "rt1.h"

t_obj	*init_cone()
{
  t_obj	*new_cone;

  if (!(new_cone = xmalloc(sizeof(t_obj))))
    return (NULL);
  new_cone->set_x = cone_set_x;
  new_cone->set_y = cone_set_y;
  new_cone->set_z = cone_set_z;
  new_cone->vector_x = cone_vector_x;
  new_cone->vector_y = cone_vector_y;
  new_cone->vector_z = cone_vector_z;
  new_cone->set_color = cone_set_color;
  new_cone->set_extra = cone_set_angle;
  new_cone->set_bril = cone_set_bril;
  new_cone->inter = inter_cone;
  new_cone->get_normal = normal_cone;
  new_cone->destroy = destroy_objs;
  return (new_cone);
}

int	cone_set_x(t_obj *cone, double value)
{
  cone->x = value;
  return (0);
}

int	cone_set_y(t_obj *cone, double value)
{
  cone->y = value;
  return (0);
}

int	cone_set_z(t_obj *cone, double value)
{
  cone->z = value;
  return (0);
}

int	cone_set_angle(t_obj *cone, double value)
{
  cone->extra = DEGTORAD(value);
  return (0);
}
