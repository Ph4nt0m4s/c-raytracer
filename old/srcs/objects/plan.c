/*
** sphere.c for raytracer in
**  /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:21:13 2015 Gabriel CADET
** Last update Fri Jun  5 22:56:02 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "xfiles.h"
#include "rt1.h"

t_obj	*init_plan()
{
  t_obj	*new_plan;

  if (!(new_plan = xmalloc(sizeof(t_obj))))
    return (NULL);
  new_plan->set_x = plan_set_x;
  new_plan->set_y = plan_set_y;
  new_plan->set_z = plan_set_z;
  new_plan->set_color = plan_set_color;
  new_plan->vector_x = plan_vector_x;
  new_plan->vector_y = plan_vector_y;
  new_plan->vector_z = plan_vector_z;
  new_plan->set_bril = plan_set_bril;
  new_plan->inter = inter_plan;
  new_plan->get_normal = normal_plan;
  new_plan->destroy = destroy_objs;
  return (new_plan);
}

int	plan_set_x(t_obj *plan, double value)
{
  plan->x = value;
  return (0);
}

int	plan_set_y(t_obj *plan, double value)
{
  plan->y = value;
  return (0);
}

int	plan_set_z(t_obj *plan, double value)
{
  plan->z = value;
  return (0);
}

int	plan_set_color(t_obj *plan, int color)
{
  plan->color = color;
  return (0);
}
