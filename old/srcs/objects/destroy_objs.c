/*
** destroy_objs.c for raytracer in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:40:01 2015 Gabriel CADET
** Last update Fri Mar 13 13:27:11 2015 Gabriel CADET
*/

#include <stdlib.h>
#include "rt1.h"

void	destroy_objs(t_obj **objs)
{
  if (*objs)
    free(*objs);
  *objs = NULL;
}
