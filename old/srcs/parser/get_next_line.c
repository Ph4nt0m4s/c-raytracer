/*
** get_next_line.c for raytracer in
** /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Tue May 26 17:15:47 2015 Joffrey Mazier
** Last update Sat Jun 06 19:04:26 2015 Gabriel CADET
*/

#include <limits.h>
#include <unistd.h>
#include <stdlib.h>
#include "get_next_line.h"
#include "rt1.h"

/*
** La fonction GET_NEXT_LINE permet de retourner un fichier passé en
** paramettre sous la forme de chaines de caractères,
** contenant chacune une ligne, (caractère '\n' exclu).
*/

/*
** Cette fonction est chargée de copier la chaine passée en premier
** paramettre dans la chaine passée en second paramètre, puis
** le contenu du buffer à la suite de l'eventuelle copie.
** les verifications de nullité sont faites sur
** les différentes chaines utilisées, et les free incidents sont effectués.
** La nouvelle chaine remplie est renvoyée à la fonction my_alloc qui
** effectuera une nouvelle boucle ou retournera son resultat
** à la fonction get_next_line.
*/
unsigned char		*my_strcopy(unsigned char *str, unsigned char *strdup,
				    unsigned char *buff, unsigned long count)
{
  unsigned long		i;
  unsigned long		buff_cpt;

  buff_cpt = 0;
  (str == NULL) ?  (i = 0) : (i = -1);
  if (buff == NULL || strdup == NULL)
    {
      free(buff);
      return (NULL);
    }
  strdup[count] = '\0';
  if (str != NULL)
    {
      while (str[++i] != '\0')
	strdup[i] = str[i];
      strdup[i] = '\0';
      free(str);
    }
  while (buff[buff_cpt] != '\0')
    strdup[i++] = buff[buff_cpt++];
  strdup[i] = '\0';
  str = strdup;
  free(buff);
  return (str);
}

/*
** Fonction d'allocation et de réallocation memoire.
** Elle est chargée d'effectuer les différents read nécéssaires
** et d'attribuer suffisament de memoire afin de stocker
** tous les read dans la variable statique : save.
** le stockage des informations dans save est effectué par la fonction
** my_strcopy, celle ci effectue des verification sur le contenu
** des chaines de caractères avant toute operation.
** A la fin de son execution la fonction retourne
** la static : save, dans la fonction de distribution : get_next_line,
** où elle subira les traitements nécéssaires au programme.
*/

unsigned char		*my_alloc(int fd, unsigned char *str)
{
  unsigned long		count[2];
  unsigned char		*buff;
  unsigned char		*strdup;

  count[0] = 0;
  count[1] = 0;
  if ((buff = malloc((sizeof(unsigned char)) * (SIZE + 2))) == NULL)
    return (NULL);
  while ((signed)(count[1] = read(fd, buff, SIZE)) > 0)
    {
      if (count[0] >=  ULONG_MAX - (count[1] + 1))
	return (NULL);
      strdup = NULL;
      buff[count[1]++] = '\0';
      count[0] = (count[0] + count[1]) - 1;
      if ((strdup = malloc((sizeof(unsigned char)) * (count[0] + 2))) == NULL)
	return (NULL);
      if ((str = my_strcopy(str, strdup, buff, count[0])) == NULL)
	return (NULL);
      if ((buff = malloc((sizeof(unsigned char)) * (SIZE + 2))) == NULL)
	return (NULL);
    }
  free(buff);
  return (str);
}

/*
** Cette fonction est chargée des traitements sur les variables :
** save, et la chaine retournée à la fonction appelante.
** La variable save y est lue jusq'à son '\n', puis copiée dans
** une chaine après lui avoir allouer une plage mémoire suffisante.
** Le compteur static : cpt est incréménté jusqu'à la position correspondant
** au dernier '\n' rencontré, afin de retouver cette position
** lors d'un futur appel à cette fonction.
** Cette fonction retourne alors la chaine représentant une unique ligne,
** à la fonction get_next_line, puis vers la fonction appelante.
*/
unsigned char		*my_next_line(unsigned char *str, unsigned char *save,
			      unsigned long *cpt, int test)
{
  unsigned long		i;
  unsigned long		size_tmp;
  static unsigned long	size_line;

  size_tmp = cpt[0];
  if (save[cpt[0]] == '\0')
    return (NULL);
  if ((str = malloc((sizeof(unsigned char)) *
		    ((my_nstrlen(save + (cpt[0]), '\n')) + 1))) == NULL)
    return (NULL);
  i = 0;
  while (save[cpt[0]] != '\n' && save[cpt[0]] != '\0')
    str[i++] = save[cpt[0]++];
  str[i] = '\0';
  if (size_line == 0 && test == 1)
    size_line = cpt[0] - size_tmp;
  if (save[cpt[0]] == '\n')
    cpt[0]++;
  return (str);
}

/*
** Fontion de lancement du GET_NEXT_LINE.
** Elle est chargée de gérer les erreurs du define SIZE disponible
** dans le fichier get_next_line.h.
** Elle est chargée de la distribution des appels de fonctions,
** de la liberation mémoire de la variable static (lorseque c'est nécéssaire)
** et du retour de la chaine de caractère contenant
** la ligne devant être retournée.
** LA VARIABLE STATIC : save contiendra dès le premier appel la totalité des
** lignes passées par le pointeur sur fichier.
** Elle sera lue à chaque tours jusqu'à son '\0'
** LE COMPTEUR STATIC : cpt, est utilisé afin de conserver d'un appel
** à l'autre la dernière position retournée par la chaine : str.
*/

char			*get_next_line(const int fd, int mode)
{
  unsigned char		*str;
  static unsigned char	*save;
  static unsigned long	cpt[1];
  int			test;

  str = NULL;
  test = 1;
  if (SIZE <= 0)
    return (NULL);
  if (save == NULL)
    {
      test = 0;
      cpt[0] = 0;
      save = my_alloc(fd, str);
      if (save == NULL)
	return (NULL);
    }
  if (save[cpt[0]] == '\0' || mode == 1)
    {
      free(save);
      cpt[0] = 0;
      save = NULL;
      return (NULL);
    }
  return ((char *)(str = my_next_line(str, save, cpt, test)));
}
