/*
** gt_obj_carac.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 22:00:07 2015 Joffrey Mazier
** Last update Sat Jun  6 17:11:43 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "rt1.h"
#include "define_file.h"

int		gt_extra(t_pars *pars)
{
  char		*str;

  if (pars->obj_n[pars->ext] < 0)
    return (0);
  if ((str = get_next_line(pars->fd, 0)) == NULL)
    return (0);
  if (pars->ext == OBJECT)
    pars->obj[pars->obj_n[pars->ext]]->extra = atof(str);
  else
    return (my_puterror("Error. Value brillance is not needed\
 for this object..\n"));
  free(str);
  pars->mask ^= O_EXTRA;
  return (0);
}

int		gt_vector(t_pars *pars)
{
  int		i;
  char		*str;

  i = 0;
  if (pars->obj_n[pars->ext] < 0)
    return (0);
  while (i < pars->get_carac[pars->tag_type])
    {
      if ((str = get_next_line(pars->fd, 0)) == NULL)
	return (0);
      if ((check_value_pos(str)) == -1)
	return (0);
      if (pars->ext == OBJECT && i == 0)
	pars->obj[pars->obj_n[pars->ext]]->vx = atof(str);
      else if (pars->ext == OBJECT && i == 1)
	pars->obj[pars->obj_n[pars->ext]]->vy = atof(str);
      else if (pars->ext == OBJECT && i == 2)
	pars->obj[pars->obj_n[pars->ext]]->vz = atof(str);
      else
	return (my_puterror("Error, vector value not needed for object..\n"));
      free(str);
      i++;
    }
  pars->mask ^= O_VECTOR;
  return (0);
}

int		gt_color(t_pars *pars)
{
  char		*str;

  if (pars->obj_n[pars->ext] < 0)
    return (0);
  if ((str = get_next_line(pars->fd, 0)) == NULL)
    return (0);
  if (pars->ext == OBJECT)
    pars->obj[pars->obj_n[pars->ext]]->color = strtol(str, NULL, 16);
  else if (pars->ext == LUX)
    pars->spot[pars->obj_n[pars->ext]]->color = strtol(str, NULL, 16);
  else
    return (my_puterror("Error. Value color is not needed for this\
 object..\n"));
  free(str);
  pars->mask ^= O_COLOR;
  return (0);
}

int		gt_brillance(t_pars *pars)
{
  char		*str;

  if (pars->obj_n[pars->ext] < 0)
    return (0);
  if ((str = get_next_line(pars->fd, 0)) == NULL)
    return (0);
  if (pars->ext == OBJECT)
    pars->obj[pars->obj_n[pars->ext]]->brillance = atof(str);
  else
    return (my_puterror("Error. Value brillance is not needed\
 for this object..\n"));
  free(str);
  pars->mask ^= O_BRILLANCE;
  return (0);
}
