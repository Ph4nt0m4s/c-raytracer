/*
** gt_limit.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:57:41 2015 Joffrey Mazier
** Last update Fri Jun  5 22:03:38 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include <stdio.h>
#include "define_file.h"
#include "rt1.h"

int		gt_limit_mode(t_pars *pars)
{
  char		*str;

  if (pars->obj_n[pars->ext] < 0)
    return (0);
  if ((str = get_next_line(pars->fd, 0)) == NULL)
    return (0);
  if (pars->ext == OBJECT)
    pars->obj[pars->obj_n[pars->ext]]->limit[1] = atof(str);
  else
    return (my_puterror("Error. Value brillance is not needed\
 for this object..\n"));
  free(str);
  pars->mask ^= O_LIMIT_MODE;
  return (0);
}

int		gt_limit(t_pars *pars)
{
  char		*str;

  if (pars->obj_n[pars->ext] < 0)
    return (0);
  if ((str = get_next_line(pars->fd, 0)) == NULL)
    return (0);
  if (pars->ext == OBJECT)
    pars->obj[pars->obj_n[pars->ext]]->limit[0] = atof(str);
  else
    return (my_puterror("Error. Value brillance is not needed\
 for this object..\n"));
  free(str);
  pars->mask ^= O_LIMIT;
  return (0);
}
