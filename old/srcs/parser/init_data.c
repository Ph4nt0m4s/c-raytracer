/*
** init_data.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 22:13:13 2015 Joffrey Mazier
** Last update Sat Jun 06 00:44:26 2015 Gabriel CADET
*/

#include <stdlib.h>
#include "rt1.h"
#include "define_file.h"

int		init_data_spot(t_pars *pars)
{
  int		i;

  i = 0;
  if ((pars->spot = malloc((sizeof(t_spot *)) * (pars->nb_obj + 2))) == NULL)
    return (my_puterror("Error with malloc(), tab nb_obj\n"));
  while (i < pars->nb_obj)
    if ((pars->spot[i++] = malloc((sizeof(t_spot)) * 1)) == NULL)
      return (my_puterror("Error with malloc(), tab nb_obj\n"));
  pars->spot[i] = NULL;
  return (0);
}

int		init_data_eye(t_pars *pars)
{
  int		i;

  i = 0;
  if ((pars->eye = malloc((sizeof(t_eye *)) * (pars->nb_obj + 2))) == NULL)
    return (my_puterror("Error with malloc(), tab nb_obj\n"));
  while (i < pars->nb_obj)
    if ((pars->eye[i++] = malloc((sizeof(t_eye)) * 1)) == NULL)
      return (my_puterror("Error with malloc(), tab nb_obj\n"));
  pars->eye[i] = NULL;
  return (0);
}

int		init_data_obj(t_pars *pars)
{
  if ((pars->obj = malloc((sizeof(t_obj *)) * (pars->nb_obj + 2))) == NULL)
    return (my_puterror("Error with malloc(), tab nb_obj\n"));
  return (0);
}

int		init_data_struct(t_pars *pars)
{
  if (pars->ext == OBJECT)
    {
      if ((init_data_obj(pars)) == -1)
        return (-1);
    }
  else if (pars->ext == CAMERA)
    {
      if ((init_data_eye(pars)) == -1)
        return (-1);
    }
  else if (pars->ext == LUX)
    {
      if ((init_data_spot(pars)) == -1)
        return (-1);
    }
  return (0);
}
