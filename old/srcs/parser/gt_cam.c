/*
** gt_cam.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:54:38 2015 Joffrey Mazier
** Last update Fri Jun  5 22:04:04 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "define_file.h"
#include "rt1.h"

int		gt_fov(t_pars *pars)
{
  char		*str;

  if (pars->obj_n[pars->ext] < 0)
    return (0);
  if ((str = get_next_line(pars->fd, 0)) == NULL)
    return (0);
  if (pars->ext == CAMERA)
    pars->eye[pars->obj_n[pars->ext]]->fov = atof(str);
  else
    return (my_puterror("Error. Fov value is not needed for object..\n"));
  free(str);
  pars->mask ^= O_FOV;
  return (0);
}

int		gt_dist(t_pars *pars)
{
  char		*str;

  if (pars->obj_n[pars->ext] < 0)
    return (0);
  if ((str = get_next_line(pars->fd, 0)) == NULL)
    return (0);
  if (pars->ext == CAMERA)
    pars->eye[pars->obj_n[pars->ext]]->dist = atof(str);
  else
    return (my_puterror("Error, dist value is not needed for\
 this object..\n"));
  free(str);
  pars->mask ^= O_DIST;
  return (0);
}

int		gt_angle(t_pars *pars)
{
  int		i;
  char		*str;

  i = 0;
  if (pars->obj_n[pars->ext] < 0)
    return (0);
  while (i < pars->get_carac[pars->tag_type])
    {
      if ((str = get_next_line(pars->fd, 0)) == NULL)
        return (0);
      if ((check_value_pos(str)) == -1)
        return (0);
      if (pars->ext == CAMERA && i == 0)
        pars->eye[pars->obj_n[pars->ext]]->anglex = atof(str);
      else if (pars->ext == CAMERA && i == 1)
        pars->eye[pars->obj_n[pars->ext]]->angley = atof(str);
      else if (pars->ext == CAMERA && i == 2)
        pars->eye[pars->obj_n[pars->ext]]->anglez = atof(str);
      else
        return (my_puterror("Error, angle not needed for this object..\n"));
      i++;
    }
  pars->mask ^= O_ANGLE;
  return (0);
}
