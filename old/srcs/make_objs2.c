/*
** make_objs.c for rtV1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Sat Mar  7 19:51:18 2015 Gabriel CADET
** Last update Sun Mar 22 20:58:56 2015 Gabriel CADET
*/

#include <stdio.h>
#include <stdlib.h>
#include "rt1.h"

t_obj		*sphere_1()
{
  double	coord[3];
  int		color;
  double	extra[2];

  coord[0] = 0;
  coord[1] = 50;
  coord[2] = 30;
  extra[0] = 0.5;
  extra[1] = 10;
  color = 0x0AF078;
  return (make_sphere(coord, NULL, color, extra));
}

t_obj		*sphere_2()
{
  double	coord[3];
  int		color;
  double	extra[2];

  coord[0] = 0;
  coord[1] = -25;
  coord[2] = 50;
  extra[0] = 0.5;
  extra[1] = 5;
  color = 0x808080;
  return (make_sphere(coord, NULL, color, extra));
}

t_obj		*cylinder_1()
{
  double	coord[3];
  double	vector[3];
  double	extra[2];
  int		color;

  coord[0] = 0;
  coord[1] = 0;
  coord[2] = 25;
  vector[0] = 0;
  vector[1] = 0;
  vector[2] = 1;
  extra[0] = 1;
  extra[1] = 10;
  color = 0xFFFFFF;
  return (make_cylinder(coord, vector, color, extra));
}

t_obj		*cone_1()
{
  double	coord[3];
  double	vector[3];
  double	extra[2];
  int		color;

  coord[0] = 100;
  coord[1] = 0;
  coord[2] = 50;
  vector[0] = 0;
  vector[1] = 1;
  vector[2] = 0;
  extra[0] = 1;
  extra[1] = 10;
  color = 0xFFFFFF;
  return (make_cone(coord, vector, color, extra));
}
