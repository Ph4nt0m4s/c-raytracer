/*
** raytracing.c for Raytracer in
** /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Feb 23 23:44:43 2015 Gabriel CADET
** Last update Tue Jul 05 13:42:54 2016 Gabriel CADET
*/

#include <unistd.h>
#include <stdlib.h>
#include "mlx.h"
#include "rt1.h"

double		calc(int *coord, t_eye *eye, t_obj *obj)
{
  double	k;
  int		x;
  int		y;

  x = coord[0];
  y = coord[1];
  eye->vector_x(eye, x, y);
  eye->vector_y(eye, x, y);
  eye->vector_z(eye, x, y);
  eye->rota_x(eye, eye->anglex);
  eye->rota_y(eye, eye->angley);
  eye->rota_z(eye, eye->anglez);
  k = obj->inter(eye, obj);
  if (k >= 0.00)
    return (k);
  else
    return (-1);
}

void		get_coord(int *coord, double *point, t_eye *eye, t_obj *obj)
{
  double	k;

  if (!obj)
    return ;
  k = calc(coord, eye, obj);
  point[0] = eye->x + eye->vx * k;
  point[1] = eye->y + eye->vy * k;
  point[2] = eye->z + eye->vz * k;
}

t_obj		*get_cur(int *coord, t_eye *eye, t_obj **obj)
{
  int		i;
  double	k;
  int		stored;
  double	ktmp;

  i = -1;
  k = -1;
  stored = -1;
  while (obj[++i])
    if ((ktmp = calc(coord, eye, obj[i])) > 0)
      {
	if (k >= 0)
	  {
	    if ((k = MIN(k, ktmp)) == ktmp)
	      stored = i;
	  }
	else
	  {
	    k = ktmp;
	    stored = i;
	  }
      }
  if (k == -1)
    return (NULL);
  return (obj[stored]);
}

int		get_cur_color(double *point, t_obj **objs,
			      t_obj *cur, t_spot **spot)
{
  int		color;

  if (!cur)
    return (0x000000);
  color = get_lumin(point, objs, cur, spot);
  return (color);
}

int		get_image(t_img *img, t_eye *eye, t_obj **obj, t_spot **spots)
{
  int		coord[2];
  double	point[3];
  t_obj		*cur;
  int		color;

  coord[0] = -1;
  while (++coord[0] < XWIN)
    {
      coord[1] = -1;
      if (coord[0] % ((img->faster == 1) ? 1 : 2) == 0)
	while (++coord[1] < YWIN)
	  if (coord[1] % (img->faster) == 0)
	    {
	      cur = get_cur(coord, eye, obj);
	      get_coord(coord, point, eye, cur);
	      color = get_cur_color(point, obj, cur, spots);
	      put_pxl_to_img(img, coord[0], coord[1], color);
	    }
	  else
	    put_pxl_to_img(img, coord[0], coord[1], color);
      mlx_put_image_to_window(img->mlx, img->win, img->img, 0, 0);
      aff_percent(coord[0]);
    }
  return (0);
}
