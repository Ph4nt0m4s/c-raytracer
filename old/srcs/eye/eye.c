/*
** eye.c for raytracer in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:21:13 2015 Gabriel CADET
** Last update Fri Jun  5 22:50:42 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "xfiles.h"
#include "rt1.h"

t_eye	*init_eye(void)
{
  t_eye	*new_eye;

  if (!(new_eye = xmalloc(sizeof(t_eye))))
    return (NULL);
  new_eye->set_x = eye_set_x;
  new_eye->set_y = eye_set_y;
  new_eye->set_z = eye_set_z;
  new_eye->vector_x = eye_vector_x;
  new_eye->vector_y = eye_vector_y;
  new_eye->vector_z = eye_vector_z;
  new_eye->rota_x = rota_x;
  new_eye->rota_y = rota_y;
  new_eye->rota_z = rota_z;
  return (new_eye);
}

int	eye_set_x(t_eye *eye, double value)
{
  eye->x = value;
  return (0);
}

int	eye_set_y(t_eye *eye, double value)
{
  eye->y = value;
  return (0);
}

int	eye_set_z(t_eye *eye, double value)
{
  eye->z = value;
  return (0);
}

int	eye_vector_x(t_eye *eye, int x, int y)
{
  (void) x;
  (void) y;
  eye->vx = (double)eye->dist;
  return (0);
}
