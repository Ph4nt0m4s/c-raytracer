/*
** xclose.c for xfiles in /home/cadet_g/source/xfiles
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Wed Jan 21 13:34:00 2015 Gabriel CADET
** Last update Mon Feb 23 23:39:28 2015 Gabriel CADET
*/

#include <unistd.h>
#include "xfiles.h"

int	xclose(int fd)
{
  if (close(fd))
    xwrite(2, "Error : could not close the file. Aborting.\n", 44);
  return (0);
}
