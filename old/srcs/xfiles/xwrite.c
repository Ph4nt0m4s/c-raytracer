/*
** xwrite.c for xfiles in /home/cadet_g/source/xfiles
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Wed Jan 21 11:58:11 2015 Gabriel CADET
** Last update Mon Feb 23 23:40:16 2015 Gabriel CADET
*/

#include <unistd.h>
#include <stdlib.h>

ssize_t		xwrite(int fd, const void *buf, size_t count)
{
  ssize_t	ret;

  if ((ret = write(fd, buf, count)) < 0)
    write(2, "ERROR write : Could not write on fd\n", 36);
  return (ret);
}
