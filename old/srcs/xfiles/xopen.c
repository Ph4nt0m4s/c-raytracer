/*
x** xopen.c for xfiles in /home/cadet_g/source/xfiles
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Wed Jan 21 13:00:37 2015 Gabriel CADET
** Last update Mon Feb 23 23:39:32 2015 Gabriel CADET
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "xfiles.h"

int	xopen(const char *pathname, int flags)
{
  int	fd;

  if ((fd = open(pathname, flags)) == -1)
    {
      xwrite(2, pathname, my_strlen(pathname));
      xwrite(2, ": could not open.\n", 18);
    }
  return (fd);
}
