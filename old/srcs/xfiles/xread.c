/*
** xread.c for xfiles in /home/cadet_g/source/xfiles
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Wed Jan 21 12:53:58 2015 Gabriel CADET
** Last update Mon Feb 23 23:39:13 2015 Gabriel CADET
*/

#include <unistd.h>
#include "xfiles.h"

ssize_t		read(int fd, void *buf, size_t count)
{
  ssize_t	ret;

  if ((ret = read(fd, buf, count)) == -1)
    xwrite(2, "Error : Read fail.");
  return (ret);
}
