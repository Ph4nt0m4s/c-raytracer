/*
** xmalloc.h for xfiles in /home/cadet_g/source/xfiles
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Wed Jan 21 12:29:30 2015 Gabriel CADET
** Last update Mon Feb 23 23:39:19 2015 Gabriel CADET
*/

#include <stdlib.h>
#include "xfiles.h"

void	*xmalloc(size_t size)
{
  void	*ptr;

  if (!(ptr = malloc(size)))
    xwrite(2, "ERROR : Malloc failed, aborting.\n", 33);
  return (ptr);
}
