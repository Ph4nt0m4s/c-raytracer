/*
** get_normal.c for rtv1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Mar  9 19:45:35 2015 Gabriel CADET
** Last update Sun Jun  7 11:41:43 2015 Joffrey Mazier
*/

#include <stdio.h>
#include <math.h>
#include "rt1.h"

int	normal_plan(t_obj *plan, double *vector, double *coord)
{
  (void) coord;
  vector[0] = plan->vx;
  vector[1] = plan->vy;
  vector[2] = plan->vz;
  return (0);
}

int	normal_sphere(t_obj *sphere, double *vector, double *coord)
{
  vector[0] = (coord[0] - sphere->x) / sphere->extra;
  vector[1] = (coord[1] - sphere->y) / sphere->extra;
  vector[2] = (coord[2] - sphere->z) / sphere->extra;
  return (0);
}

int	normal_cylinder(t_obj *cy, double *vector, double *coord)
{
  double	vtmp[3];
  double	scal;
  double	pts[3];
  double	norm;

  vtmp[0] = coord[0] - cy->x;
  vtmp[1] = coord[1] - cy->y;
  vtmp[2] = coord[2] - cy->z;
  scal = vtmp[0] * cy->vx + vtmp[1] * cy->vy + vtmp[2] * cy->vz;
  norm = sqrt(cy->vx * cy->vx + cy->vy * cy->vy + cy->vz *cy->vz);
  scal /= norm;
  pts[0] = cy->x + (scal / norm) * cy->vx;
  pts[1] = cy->y + (scal / norm) * cy->vy;
  pts[2] = cy->z + (scal / norm) * cy->vz;
  vector[0] = coord[0] - pts[0];
  vector[1] = coord[1] - pts[1];
  vector[2] = coord[2] - pts[2];
  return (0);
}

int	normal_cone(t_obj *co, double *vector, double *coord)
{
  double	vtmp[3];
  double	scal;
  double	pts[3];
  double	norm;
  double	dist;

  vtmp[0] = coord[0] - co->x;
  vtmp[1] = coord[1] - co->y;
  vtmp[2] = coord[2] - co->z;
  norm = sqrt(co->vx * co->vx + co->vy * co->vy + co->vz *co->vz);
  scal = (vtmp[0] * co->vx + vtmp[1] * co->vy + vtmp[2] * co->vz) / norm;
  pts[0] = co->x + (scal / norm) * co->vx;
  pts[1] = co->y + (scal / norm) * co->vy;
  pts[2] = co->z + (scal / norm) * co->vz;
  dist = sqrt(SQR(coord[0] - pts[0]) + SQR(coord[1] - pts[1])
              + SQR(coord[2] - pts[2]));
  dist = dist * tan(co->extra) + scal;
  pts[0] = co->x + (scal / norm) * co->vx;
  pts[1] = co->y + (scal / norm) * co->vy;
  pts[2] = co->z + (scal / norm) * co->vz;
  vector[0] = coord[0] - pts[0];
  vector[1] = coord[1] - pts[1];
  vector[2] = coord[2] - pts[2];
  return (0);
}
