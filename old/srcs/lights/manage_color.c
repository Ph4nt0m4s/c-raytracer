/*
** manage_color.c for rt1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/lights
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Mar  9 22:46:39 2015 Gabriel CADET
** Last update Thu Mar 12 01:04:30 2015 Gabriel CADET
*/

#include <stdio.h>
#include "rt1.h"

void		get_obj_color(t_obj *obj, int *color)
{
  unsigned char  *rgb;
  int		endian;
  int		i;

  i = -1;
  rgb = (unsigned char *) &(obj->color);
  endian = is_little_endian();
  if (!(endian))
    while (++i < 3)
      color[i] = rgb[i + 1] * ((double) 1 - obj->brillance);
  else
    while (++i < 3)
      color[2 - i] = rgb[i] * ((double) 1 - obj->brillance);
}

int		make_color(int *color)
{
  int		i;
  int		new_color;
  unsigned char  *rgb;
  char		endian;

  i = -1;
  rgb = (unsigned char *) &new_color;
  endian = is_little_endian();
  if (!(endian))
    while (++i < 3)
      rgb[i + 1] = color[i];
  else
    while (++i < 3)
      rgb[i] = color[2 - i];
  return (new_color);
}

int		light_to_int(t_spot *spot, int *color)
{
  unsigned char  *rgb;
  int		endian;
  int		i;

  i = -1;
  rgb = (unsigned char *) &(spot->color);
  endian = is_little_endian();
  if (!(endian))
    while (++i < 3)
      color[i] = rgb[i + 1];
  else
    while (++i < 3)
      color[2 - i] = rgb[i];
  return (0);
}

int	apply_color(t_obj *obj, int *out_col, double *lumin, int nspot)
{
  int	tmp_color[3];
  int	i;

  i = -1;
  if (nspot == 0)
    {
      while (++i < 3)
	out_col[i] += 0;
      return (-1);
    }
  get_obj_color(obj, tmp_color);
  while (++i < 3)
    out_col[i] /= nspot;
  i = -1;
  while (++i < 3)
    out_col[i] += tmp_color[i];
  i = -1;
  while (++i < 3)
    out_col[i] *= (lumin[i] / nspot);
  return (0);
}
