/*
** make_objs.c for rtV1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Sat Mar  7 19:51:18 2015 Gabriel CADET
** Last update Sun Mar 22 20:34:48 2015 Gabriel CADET
*/

#include <stdlib.h>
#include "rt1.h"

t_obj		*plan_1()
{
  double	coord[3];
  double	vector[3];
  double	extra[1];
  int		color;

  coord[0] = 0;
  coord[1] = 0;
  coord[2] = 0;
  vector[0] = 0;
  vector[1] = 0;
  vector[2] = 1;
  extra[0] = 1;
  color = 0xFFFFFF;
  return (make_plan(coord, vector, color, extra));
}

t_obj		*plan_2()
{
  double	coord[3];
  double	vector[3];
  double	extra[1];
  int		color;

  coord[0] = 500;
  coord[1] = 0;
  coord[2] = 0;
  vector[0] = -1;
  vector[1] = 0;
  vector[2] = 0;
  extra[0] = 1;
  color = 0xFFFFFF;
  return (make_plan(coord, vector, color, extra));
}

t_obj	**make_obj()
{
  t_obj	**object;
  int	i;

  i = -1;
  if (!(object = malloc(sizeof (t_obj *) * 7)))
    return (NULL);
  object[0] = plan_1();
  object[1] = sphere_1();
  object[2] = plan_2();
  object[3] = sphere_2();
  object[4] = cylinder_1();
  object[5] = cone_1();
  object[6] = NULL;
  while (++i < 6)
    if (!object[i])
      return (NULL);
  return (object);
}
