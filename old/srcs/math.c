/*
** math.c for rt1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Mar  9 21:19:21 2015 Gabriel CADET
** Last update Wed Mar 11 17:04:28 2015 Gabriel CADET
*/

#include <math.h>
#include "rt1.h"

double		calc_norm(double *vector)
{
  double	norm;
  double	x;
  double	y;
  double	z;

  x = vector[0];
  y = vector[1];
  z = vector[2];
  norm = sqrt(x * x + y * y + z * z);
  return (norm);
}

double		calc_scalaire(double *vector1, double *vector2)
{
  double	scal;
  double	x1;
  double	y1;
  double	z1;
  double	x2;
  double	y2;
  double	z2;

  x1 = vector1[0];
  y1 = vector1[1];
  z1 = vector1[2];
  x2 = vector2[0];
  y2 = vector2[1];
  z2 = vector2[2];
  scal = x1 * x2 + y1 * y2 + z1 * z2;
  return (scal);
}
