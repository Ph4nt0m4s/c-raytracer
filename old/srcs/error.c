/*
** error.c for raytracer in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Feb 23 23:13:38 2015 Gabriel CADET
** Last update Sun Mar 15 12:43:44 2015 Gabriel CADET
*/

#include <stdlib.h>
#include "rt1.h"

void	*error_mlx()
{
  my_putstrf("Could not initialize the mlx.\nAborting\n", 2);
  return (NULL);
}
