/*
** rt1.h for raytracer in /home/mazier_j/MUL_2014_rtracer
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Sat Jun  6 17:48:39 2015 Joffrey Mazier
** Last update Sun Jun  7 19:31:13 2015 Joffrey Mazier
*/

#ifndef RT1_H_
# define RT1_H_

# include <stdio.h>

# define PI		(3.14159265359)
# define DEGTORAD(x)	((x) * PI / 180)
# define XWIN		1000
# define YWIN		1000
# define WIN_NAME	"Raytracer"
# define MIN(x, y)	((x) <= (y) ? (x) : (y))
# define MAX(x, y)	((x) >= (y) ? (x) : (y))
# define ABS(x)		((x) >= 0 ? (x) : -(x))
# define SIGN(x)	((x) >= 0 ? 0 : 1)
# define SQR(x)		((x) * (x))

typedef struct	s_img
{
  void		*mlx;
  void		*win;
  void		*img;
  int		bpp;
  int		endian;
  int		line;
  char		*addr;
  int		faster;
  int		cam;
}		t_img;

typedef struct	s_obj t_obj;
typedef struct	s_spot t_spot;
typedef struct	s_eye t_eye;

typedef struct s_eve
{
  int		select[2];
  int		mode;
}		t_eve;

typedef struct	s_pars
{
  char		**carac;
  int		*get_carac;
  int		fd;
  int		obj_n[4];
  int		tag_type;
  int		type;
  int		ext;
  int		nb_obj;
  int		nb_lux;
  int		nb_cam;
  int		nb_object;
  int		end;
  int		mask;
  char		*path;
  t_obj		**obj;
  t_eye		**eye;
  t_spot	**spot;
  t_img		*img;
  t_eve		*eve;
}		t_pars;

struct		s_spot
{
  double	x;
  double	y;
  double	z;
  int		color;
  char		*name;
};

struct		s_obj
{
  int		(*set_x)(t_obj *obj, double value);
  int		(*set_y)(t_obj *obj, double value);
  int		(*set_z)(t_obj *obj, double value);
  int		(*vector_x)(t_obj *obj, double value);
  int		(*vector_y)(t_obj *obj, double value);
  int		(*vector_z)(t_obj *obj, double value);
  int		(*set_extra)(t_obj *obj, double value);
  int		(*set_bril)(t_obj *obj, double value);
  int		(*set_color)(t_obj *obj, int value);
  double	(*inter)(t_eye *eye, t_obj *obj);
  int		(*get_normal)(t_obj *obj, double *vector, double *coord);
  void		(*destroy)(t_obj **obj);
  double	x;
  double	y;
  double	z;
  double	vx;
  double	vy;
  double	vz;
  double	extra;
  double	refl;
  int		color;
  double	brillance;
  double	limit[2];
  char		*name;
};

/*
**	Eye Handling
*/
struct		s_eye
{
  int		(*set_x)(t_eye *eye, double value);
  int		(*set_y)(t_eye *eye, double value);
  int		(*set_z)(t_eye *eye, double value);
  double	(*rota_x)(t_eye *eye, double value);
  double	(*rota_y)(t_eye *eye, double value);
  double	(*rota_z)(t_eye *eye, double value);
  int		(*vector_x)(t_eye *eye, int x, int y);
  int		(*vector_y)(t_eye *eye, int x, int y);
  int		(*vector_z)(t_eye *eye, int x, int y);
  double	x;
  double	y;
  double	z;
  double	vx;
  double	vy;
  double	vz;
  double	anglex;
  double	angley;
  double	anglez;
  double	dist;
  double	fov;
  char		*name;
};

t_eye		*init_eye(void);
t_eye		*make_eye(double *coord, double dist,
			  double fov, double *angle);
int		eye_set_x(t_eye *eye, double value);
int		eye_set_y(t_eye *eye, double value);
int		eye_set_z(t_eye *eye, double value);
int		eye_vector_x(t_eye *eye, int x, int y);
int		eye_vector_y(t_eye *eye, int x, int y);
int		eye_vector_z(t_eye *eye, int x, int y);
double		rota_x(t_eye *eye, double value);
double		rota_y(t_eye *eye, double value);
double		rota_z(t_eye *eye, double value);

/*
**		Sphere Handling
*/
t_obj		*init_sphere();
t_obj		*make_sphere(double *coord, double *vector,
			     int color, double *extra);
int		sphere_set_x(t_obj *sphere, double value);
int		sphere_set_y(t_obj *sphere, double value);
int		sphere_set_z(t_obj *sphere, double value);
int		sphere_set_radius(t_obj *sphere, double value);
int		sphere_set_bril(t_obj *sphere, double value);
int		sphere_set_color(t_obj *sphere, int value);
int		sphere_vector_x(t_obj *sphere, double value);
int		sphere_vector_y(t_obj *sphere, double value);
int		sphere_vector_z(t_obj *sphere, double value);
double		inter_sphere(t_eye *eye, t_obj *sphere);
int		normal_sphere(t_obj *sphere, double *vector, double *coord);
double		get_scal(t_eye *eye, double dist, t_obj *obj);
double		inter_limited(t_eye *eye, t_obj *sphere,
			      double dist, double *arg);
double		rota_x_obj(t_obj *obj, double angle);
double		rota_y_obj(t_obj *obj, double angle);
double		rota_z_obj(t_obj *obj, double angle);

/*
**	Plane Handling
*/
t_obj		*init_plan();
t_obj		*make_plan(double *coord, double *vector,
			   int color, double *extra);
int		plan_set_x(t_obj *plan, double value);
int		plan_set_y(t_obj *plan, double value);
int		plan_set_z(t_obj *plan, double value);
int		plan_set_bril(t_obj *plan, double value);
int		plan_set_color(t_obj *plan, int value);
int		plan_vector_x(t_obj *plan, double value);
int		plan_vector_y(t_obj *plan, double value);
int		plan_vector_z(t_obj *plan, double value);
double		inter_plan(t_eye *eye, t_obj *plan);
int		normal_plan(t_obj *sphere, double *vector, double *coord);

/*
**	Cylinder Handling
*/
t_obj		*init_cylinder();
t_obj		*make_cylinder(double *coord, double *vector,
			       int color, double *extra);
int		cylinder_set_x(t_obj *cylinder, double value);
int		cylinder_set_y(t_obj *cylinder, double value);
int		cylinder_set_z(t_obj *cylinder, double value);
int		cylinder_set_color(t_obj *cylinder, int value);
int		cylinder_set_radius(t_obj *cylinder, double value);
int		cylinder_set_bril(t_obj *cylinder, double value);
int		cylinder_vector_x(t_obj *cylinder, double value);
int		cylinder_vector_y(t_obj *cylinder, double value);
int		cylinder_vector_z(t_obj *cylinder, double value);
double		inter_cylinder(t_eye *eye, t_obj *cylinder);
int		normal_cylinder(t_obj *sphere, double *vector, double *coord);

/*
**	Cone Handling
*/
t_obj		*init_cone();
t_obj		*make_cone(double *coord, double *vector,
			   int color, double *extra);
int		cone_set_x(t_obj *cone, double value);
int		cone_set_y(t_obj *cone, double value);
int		cone_set_z(t_obj *cone, double value);
int		cone_set_color(t_obj *cone, int value);
int		cone_set_angle(t_obj *cone, double value);
int		cone_set_bril(t_obj *cone, double value);
int		cone_vector_x(t_obj *cone, double value);
int		cone_vector_y(t_obj *cone, double value);
int		cone_vector_z(t_obj *cone, double value);
double		inter_cone(t_eye *eye, t_obj *cone);
int		normal_cone(t_obj *sphere, double *vector, double *coord);

/*
**	Light Handling
*/
t_spot		**make_light();
t_spot		*init_spot(double *coord, int color);
int		get_lumin(double *coord, t_obj **objects,
			  t_obj *obj, t_spot **spot);

t_obj		*sphere_1();
t_obj		*sphere_2();
t_obj		*cylinder_1();
t_obj		*cone_1();
void		destroy_objs(t_obj **objs);
t_obj		**make_obj();
void		get_obj_color(t_obj *obj, int *color);
int		make_color(int *color);
int		light_to_int(t_spot *spot, int *color);
int		apply_color(t_obj *obj, int *color, double *lumin, int nspot);
double		get_root(double a, double b, double delta);
double		get_root2(double a, double b, double delta);
int		get_image(t_img *img, t_eye *eye,
			  t_obj **objects, t_spot **spots);

/*
**	Mlx related Functions
*/
int		gere_expose(t_pars *util);
int		gere_key(int key, t_pars *util);
int		put_pxl_to_img(t_img *img, int x, int y, int color);
void		gere_limited(int key, t_pars *util);
void		gere_resize(int key, t_pars *util);

/*
**	Maths Functions
*/
double		calc_norm(double *vector);
double		calc_scalaire(double *vector1, double *vector2);

/*
**		Others Functions
*/
void		aff_percent(int x);
int		my_putcharf(char c, int fd);
int		my_putstrf(char *str, int fd);
int		my_putnbrf(int nbr, int fd);
int		my_strlen(char *str);
int		is_little_endian(void);
void		*error_mlx(void);
void		init_color(int *color);
void		init_lumin(double *color);
void		init_events(t_pars *util);

/*
** Functions from parser
*/

t_pars		*init_parsing(char *path_dir, t_obj **obj,
			     t_spot **spot, t_eye **eye);
char		*get_next_line(const int fd, int mode);

/*
** Functions utils
*/

int		my_nstrlen(unsigned char *str, unsigned char c);
int		my_puterror(char *str);

/*
** Function(s) in file get_carac.c
*/

int		gt_sphere(t_pars *pars);
int		gt_cylindre(t_pars *pars);
int		gt_cone(t_pars *pars);
int		gt_pos(t_pars *pars);
int		gt_color(t_pars *pars);
int		gt_brillance(t_pars *pars);
int		gt_fov(t_pars *pars);
int		gt_dist(t_pars *pars);
int		gt_angle(t_pars *pars);
int		gt_name(t_pars *pars);
int		gt_plan(t_pars *pars);
int		gt_vector(t_pars *pars);
int		gt_end(t_pars *pars);
int		gt_extra(t_pars *pars);
int		gt_limit_mode(t_pars *pars);
int		gt_limit(t_pars *pars);
int		gt_refl(t_pars *pars);

/*
** Functions GERE
*/

void		gere_key_cam(int key, t_pars *util);
void		gere_key_lux(int key, t_pars *util);
void		gere_key_obj(int key, t_pars *util);

/*
** Functions MODULE
*/

int		screen_shot_it();

/*
** Export CONF
*/

int		export_da_conf(t_pars *util);

/*
** EXPORT FUNC
*/

void		full_fill_obj(t_pars *util, FILE *fd);
void		full_fill_lux(t_pars *util, FILE *fd);
void		full_fill_cam(t_pars *util, FILE *fd);
char		*my_str_cat(char *str1, char *str2);
int		create_cam(t_pars *util, char *dir_name);
int		create_obj(t_pars *util, char *dir_name);
int		create_lux(t_pars *util, char *dir_name);

/*
** PARSER-FILE
*/

int		gt_end(t_pars *pars);
void		set_cam_default2(t_pars *pars);
void		set_cam_default(t_pars *pars);
void		set_lux_default(t_pars *pars);
int		set_obj_default(t_pars *pars);
int		check_value_pos(char *str);
char		*make_the_path(char *path_dir, char *file);
int		get_file_type(char *file);
int		init_data_struct(t_pars *pars);
int		my_str_compare(char *str, char *comp);
int		check_file_type(char *str, t_pars *pars);
int		init_carac_tab(t_pars *pars);
int		check_nb_objects(char *str);

#endif /* !RT1_H_ */
