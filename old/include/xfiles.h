/*
** xfiles.h for xfiles in /home/cadet_g/source/xfiles
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Feb 23 22:39:34 2015 Gabriel CADET
** Last update Sun Jun  7 19:31:34 2015 Joffrey Mazier
*/

#ifndef XFILES_H_
# define XFILES_H_

# include <unistd.h>

ssize_t		xwrite(int fd, const void *buf, size_t count);
ssize_t		read(int fd, void *buf, size_t count);
void		*xmalloc(size_t size);
int		xopen(const char *pathname, int flags);
int		xclose(int fd);

#endif /* !XFILES_H_ */
