/*
** ICam.cpp for Raytracer in /home/phantomas/Project/c-raytracer/eyes
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.eu>
**
** Started on  Thu Sep 29 16:11:55 2016 Gabriel CADET
** Last update Mon Oct 03 15:47:12 2016 Gabriel CADET
*/

#include "Cam.hpp"

int	raytracer::Cam::identifier = 0;

raytracer::Cam::Cam(std::string const & nam,
                    double x, double y, double z,
                    double rot_x, double rot_y, double rot_z,
                    double fov, double dist):
  id(raytracer::Cam::identifier++), coord({x, y, z}), rot({rot_x, rot_y, rot_z}),
  fov(fov), dist(dist)
{
  std::stringstream	ss;
  ss << this->id;
  if (nam.empty())
    ss << "_Camera";
  else
    ss << "_" << nam;
  this->name = ss.str();
}

void	raytracer::Cam::feedVector(double x, double y, std::array<double, 3> &feed) const
{
  feed[0] = this->dist;
  feed[1] = this->dist * tan((this->fov / XWIN) * (XWIN / 2 - y));
  feed[2] = this->dist * tan((this->fov / YWIN) * (YWIN / 2 - x));
  applyRotX(feed);
  applyRotY(feed);
  applyRotZ(feed);
}

/*** Rotations ***/

void	raytracer::Cam::applyRotX(std::array <double, 3> &vect) const
{
  double vy, vz;

  vy = vect[1];
  vz = vect[2];
  vect[1] = vy * cos(this->rot[0]) - vz * sin(this->rot[0]);
  vect[2] = vy * sin(this->rot[0]) + vz * cos(this->rot[0]);
}

void	raytracer::Cam::applyRotY(std::array <double, 3> &vect) const
{
  double vx, vz;

  vx = vect[0];
  vz = vect[2];
  vect[0] = vz * sin(this->rot[1]) + vx * cos(this->rot[1]);
  vect[2] = vz * cos(this->rot[1]) - vx * sin(this->rot[1]);
}

void	raytracer::Cam::applyRotZ(std::array <double, 3> &vect) const
{
  double vx, vy;

  vx = vect[0];
  vy = vect[1];
  vect[0] = vx * cos(this->rot[3]) - vy * sin(this->rot[3]);
  vect[2] = vx * sin(this->rot[3]) + vy * cos(this->rot[3]);
}

/*** !Rotations ***/

/*** Getter ***/

std::array<double, 3> const &	raytracer::Cam::getCoord(void) const
{
  return this->coord;
}

std::array<double, 3> const &	raytracer::Cam::getRot(void) const
{
  return this->rot;
}

double				raytracer::Cam::getFov(void) const
{
  return this->fov;
}

double				raytracer::Cam::getDist(void) const
{
  return this->dist;
}

/*** !Getter ***/

/*** Setter ***/

void	raytracer::Cam::setCoord(std::array<double, 3> const &new_coord)
{
  this->coord[0] = new_coord[0];
  this->coord[1] = new_coord[1];
  this->coord[2] = new_coord[2];
}

void	raytracer::Cam::setRot(std::array<double, 3> const &new_rot)
{
  this->rot[0] = new_rot[0];
  this->rot[1] = new_rot[1];
  this->rot[2] = new_rot[2];
}

void	raytracer::Cam::setFov(double new_fov)
{
  this->fov = new_fov;
}

void	raytracer::Cam::setDist(double new_dist)
{
  this->dist = new_dist;
}

/*** !Setter ***/

