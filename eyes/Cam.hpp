/*
** ICam.hpp for Raytracer in /home/phantomas/Project/c-raytracer/eyes
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.eu>
**
** Started on  Thu Sep 29 16:11:48 2016 Gabriel CADET
** Last update Mon Oct 03 15:47:11 2016 Gabriel CADET
*/

#ifndef CAM_HPP_
#define CAM_HPP_

#include <sstream>
#include <string>
#include <array>
#include <cmath>

namespace raytracer {
  class Cam {
    public:
      Cam(std::string const & name = "",
           double x = 0, double y = 0, double z = 0,
           double rot_x = 0, double rot_y = 0, double rot_z = 0,
           double fov = 80.0, double dist = 100.0);

      std::array<double, 3> const &	getCoord(void) const;
      std::array<double, 3> const &	getRot(void) const;
      double				getFov(void) const;
      double				getDist(void) const;

      void	setCoord(std::array<double, 3> const &);
      void	setRot(std::array<double, 3> const &);
      void	setFov(double);
      void	setDist(double);

      void	feedVector(double x, double y, std::array<double, 3> &) const;

    private:
      void	applyRotX(std::array<double, 3> &) const;
      void	applyRotY(std::array<double, 3> &) const;
      void	applyRotZ(std::array<double, 3> &) const;

    public:
      static int identifier;
    protected:
      int		id;
      std::string	name;
      std::array<double, 3>	coord;
      std::array<double, 3>	rot;
      double			fov;
      double			dist;
  };
} // namespace raytracer

#endif /* end of include guard */
