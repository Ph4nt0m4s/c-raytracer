/*
** Polynome.hpp for Math in /home/phantomas/Project/c-raytracer/math
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.eu>
**
** Started on  Sat Sep 03 19:34:34 2016 Gabriel CADET
** Last update Sun Sep 04 12:13:25 2016 Gabriel CADET
*/

#include <vector>
#include <math.h>

namespace math {
  template<class T>
    class Polynome {
      public:
        Polynome(T a, T b, T c):a(a), b(b), c(c) { this->calc(); }
        ~Polynome(){};

        Polynome & operator=(Polynome<T> const &);

        T const &		getA() const { return this->a; }
        T const &		getB() const { return this->b; }
        T const &		getC() const { return this->c; }
        T const &		getDiscriminant() const { return this->discriminant; }
        std::vector<T> const &	getRoots() const { return this->roots; }

        void	setA(T value) { this->a = value; }
        void	setB(T value) { this->b = value; }
        void	setC(T value) { this->c = value; }

        void	calc();

      private:
        T		a;
        T		b;
        T		c;
        T		discriminant;
        std::vector<T>	roots;
    };

  template<typename T>
  void	Polynome<T>::calc()
  {
    this->discriminant = this->b * this->b - 4 * this->a * this->c;
    this->roots.clear();
    if (discriminant > 0)
    {
      roots.push_back((-b - sqrt(discriminant)) / 2 * a);
      roots.push_back((-b + sqrt(discriminant)) / 2 * a);
    }
    else if (discriminant == 0) {
      roots.push_back(-b / (2 * a));
    }
  }
} // namespace math
