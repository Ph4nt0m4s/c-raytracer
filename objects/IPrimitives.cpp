/*
** IPrimitives.cpp for raytracer in /home/phantomas/Project/c-raytracer/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.eu>
**
** Started on  Sat Sep 03 16:54:45 2016 Gabriel CADET
** Last update Thu Sep 29 17:56:43 2016 Gabriel CADET
*/

#include "IPrimitives.hpp"

int 	raytracer::IPrimitives::identifier = 0;

raytracer::IPrimitives::IPrimitives(std::string const & new_name,
                                    double x, double y, double z,
                                    int color, double brillance):
id(IPrimitives::identifier++), coord({x, y, z}), color(color), brillance(brillance)
{
  std::stringstream	ss;

  ss << this->id;
  if (new_name.empty())
    ss <<  "_Unknown";
  else
    ss << "_" << new_name;
  this->name = ss.str();
}

std::string const &		raytracer::IPrimitives::getName() const
{
  return this->name;
}

std::array<double, 3> const &	raytracer::IPrimitives::getCoord() const
{
  return this->coord;
}

int				raytracer::IPrimitives::getColor() const
{
  return this->color;
}

double				raytracer::IPrimitives::getBrillance() const
{
  return this->brillance;
}

void	raytracer::IPrimitives::setName(std::string const &new_name)
{
  std::stringstream	ss;

  ss << this->id << "_" << new_name;
  this->name = ss.str();
}

void	raytracer::IPrimitives::setCoord(std::array<double, 3> const &new_coord)
{
  this->coord = new_coord;
}

void	raytracer::IPrimitives::setColor(int new_color)
{
  this->color = new_color;
}

void	raytracer::IPrimitives::setBrillance(double new_bril)
{
  this->brillance = new_bril;
}
