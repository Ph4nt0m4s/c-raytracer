/*
** IPrimitives.hpp for C++ RayTracer in /home/phantomas/Project/c-raytracer/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.eu>
**
** Started on  Fri Aug 26 17:21:14 2016 Gabriel CADET
** Last update Thu Sep 29 17:49:15 2016 Gabriel CADET
*/

#ifndef IPRIMITIVES_HPP_
#define IPRIMITIVES_HPP_

#include <sstream>
#include <string>
#include <array>

namespace raytracer
{
  class IPrimitives {
    public:
      IPrimitives(std::string const & name = "",
                  double x = 0, double y = 0, double z = 0,
                  int color = 0xFFFFFF, double brillance = 0.5);
      virtual ~IPrimitives(){};

      virtual double			intersection() const = 0;
      //virtual std::array<double, 3>	getNormal() const = 0;

      std::string const &		getName() const;
      std::array<double, 3> const &	getCoord() const;
      int 				getColor() const;
      double 				getBrillance() const;

      void				setName(std::string const &);
      void				setCoord(std::array<double, 3> const &);
      void				setColor(int);
      void				setBrillance(double);

    public:
      static int			identifier;
    protected:
      int				id;
      std::string			name;
      std::array<double, 3>		coord;
      int				color;
      double				brillance;

  };
} // !namespace raytracer

#endif /* end of include guard */
